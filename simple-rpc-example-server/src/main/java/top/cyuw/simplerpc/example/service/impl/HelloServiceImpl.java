package top.cyuw.simplerpc.example.service.impl;

import top.cyuw.simplerpc.example.service.HelloService;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author chen
 * @date 2023/3/14 21:00
 */
public class HelloServiceImpl implements HelloService {

    @Override
    public String sayHello(String msg) {
        return "[" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "] " + msg;
    }

}
