package top.cyuw.simplerpc.example.server;

import top.cyuw.simplerpc.config.RpcServiceConfig;
import top.cyuw.simplerpc.example.service.impl.HelloServiceImpl;
import top.cyuw.simplerpc.remoting.server.SimpleRpcServer;

import java.net.InetSocketAddress;
import java.util.concurrent.CountDownLatch;

/**
 * @author chen
 * @date 2023/3/14 19:55
 */
public class TestServer {

    public static void main(String[] args) {
        SimpleRpcServer rpcServer = new SimpleRpcServer();

        rpcServer.getContext().getRpcConfig().setServerPort(9977);

        RpcServiceConfig serviceConfig = RpcServiceConfig.builder()
                .version("1.0.0")
                .group("test")
                .service(new HelloServiceImpl())
                .build();

        InetSocketAddress address = new InetSocketAddress("127.0.0.1", 9977);
        rpcServer.getServiceRegistry().registerService(serviceConfig, address);

        rpcServer.start();

        CountDownLatch latch = new CountDownLatch(1);
        try {
            latch.await();
        } catch (InterruptedException ignored) {

        }
    }

}
