package top.cyuw.simplerpc.exception;

/**
 * @author chen
 * @date 2023/3/12 20:01
 */
public class RpcException extends RuntimeException {

    private static final long serialVersionUID = -7636482121312710154L;

    public RpcException(String message) {
        super(message);
    }

    public RpcException(String message, Throwable cause) {
        super(message, cause);
    }

}
