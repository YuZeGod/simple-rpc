package top.cyuw.simplerpc.exception;

/**
 * @author chen
 * @date 2023/3/12 20:03
 */
public class SerializeException extends RuntimeException {

    private static final long serialVersionUID = -7001048236191657526L;

    public SerializeException(String message, Throwable cause) {
        super(message, cause);
    }

}
