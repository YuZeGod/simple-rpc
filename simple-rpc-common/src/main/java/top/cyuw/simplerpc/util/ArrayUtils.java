package top.cyuw.simplerpc.util;

/**
 * @author chen
 * @date 2023/3/12 21:16
 */
public class ArrayUtils {

    public static boolean isEmpty(byte[] arr) {
        return arr == null || arr.length == 0;
    }

    public static boolean isEmpty(Object[] arr) {
        return arr == null || arr.length == 0;
    }

}
