package top.cyuw.simplerpc.util;

import java.util.Collection;

/**
 * @author chen
 * @date 2023/3/12 20:04
 */
public class CollectionUtils {

    public static boolean isEmpty(Collection<?> c) {
        return c == null || c.isEmpty();
    }

    public static boolean isNotEmpty(Collection<?> c) {
        return !isEmpty(c);
    }

}
