package top.cyuw.simplerpc.util;

/**
 * @author chen
 * @date 2023/3/12 20:06
 */
public class StringUtils {

    public static boolean isEmpty(CharSequence c) {
        return c == null || c.length() == 0;
    }

    public static boolean isNotEmpty(CharSequence c) {
        return !isEmpty(c);
    }

    public static String[] split(String text, String splitter) {
        if (StringUtils.isEmpty(text) || StringUtils.isEmpty(splitter)) {
            return new String[0];
        }
        return text.split(splitter);
    }

}
