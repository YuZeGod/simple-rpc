package top.cyuw.simplerpc.example.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.cyuw.simplerpc.example.spring.service.SayHelloService;

/**
 * @author chen
 * @date 2023/3/16 10:21
 */
@RestController
public class HelloController {

    @Autowired
    SayHelloService sayHelloService;

    @GetMapping("/sayHello")
    public String sayHello(@RequestParam String msg) {
        return sayHelloService.sayHello(msg);
    }

}
