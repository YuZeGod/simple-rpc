package top.cyuw.simplerpc.example.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author chen
 * @date 2023/3/16 10:25
 */
@ComponentScan("top.cyuw.simplerpc")
@SpringBootApplication
public class SimpleRpcExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleRpcExampleApplication.class, args);
    }

}
