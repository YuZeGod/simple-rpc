package top.cyuw.simplerpc.example.spring.service;

import org.springframework.stereotype.Service;
import top.cyuw.simplerpc.example.service.HelloService;
import top.cyuw.simplerpc.spring.annotation.RpcReference;

/**
 * @author chen
 * @date 2023/3/16 10:22
 */
@Service
public class SayHelloService {

    @RpcReference(version = "1.0.0", group = "test")
    HelloService helloService;

    public String sayHello(String msg) {
        return helloService.sayHello(msg);
    }

}
