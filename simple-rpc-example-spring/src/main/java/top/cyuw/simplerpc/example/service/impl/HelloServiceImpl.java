package top.cyuw.simplerpc.example.service.impl;

import org.springframework.stereotype.Component;
import top.cyuw.simplerpc.example.service.HelloService;
import top.cyuw.simplerpc.spring.annotation.RpcService;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author chen
 * @date 2023/3/14 21:00
 */
@Component
@RpcService(version = "1.0.0", group = "test")
public class HelloServiceImpl implements HelloService {

    @Override
    public String sayHello(String msg) {
        return "[" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "] " + msg;
    }

}
