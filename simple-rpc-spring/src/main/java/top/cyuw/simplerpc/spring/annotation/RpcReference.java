package top.cyuw.simplerpc.spring.annotation;

import java.lang.annotation.*;

/**
 * @author chen
 * @date 2023/3/12 21:05
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface RpcReference {

    String version() default "";

    String group() default "";

}
