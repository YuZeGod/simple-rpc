package top.cyuw.simplerpc.spring.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.cyuw.simplerpc.remoting.client.SimpleRpcClient;
import top.cyuw.simplerpc.remoting.server.SimpleRpcServer;
import top.cyuw.simplerpc.spring.RpcReferenceAnnotationPostProcessor;
import top.cyuw.simplerpc.spring.RpcServiceAnnotationPostProcessor;

/**
 * @author chen
 * @date 2023/3/16 10:05
 */
@Slf4j
@Configuration
public class SimpleRpcConfiguration {

    @Bean
    public SimpleRpcServer simpleRpcServer() {
        return new SimpleRpcServer();
    }

    @Bean
    public SimpleRpcClient simpleRpcClient() {
        SimpleRpcClient simpleRpcClient = new SimpleRpcClient();
        simpleRpcClient.start();
        return simpleRpcClient;
    }

    @Bean
    public RpcServiceAnnotationPostProcessor rpcServiceAnnotationPostProcessor(@Autowired SimpleRpcServer simpleRpcServer) {
        return new RpcServiceAnnotationPostProcessor(simpleRpcServer);
    }

    @Bean
    public RpcReferenceAnnotationPostProcessor rpcReferenceAnnotationPostProcessor(@Autowired SimpleRpcClient simpleRpcClient) {
        log.info("creating RpcReferenceAnnotationPostProcessor: " + simpleRpcClient);
        return new RpcReferenceAnnotationPostProcessor(simpleRpcClient);
    }

}
