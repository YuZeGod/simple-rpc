package top.cyuw.simplerpc.spring.annotation;

import java.lang.annotation.*;

/**
 * @author chen
 * @date 2023/3/12 21:03
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface RpcService {

    String version() default "";

    String group() default "";

}
