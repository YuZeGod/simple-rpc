package top.cyuw.simplerpc.remoting.client;

import io.netty.channel.Channel;

import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author chen
 * @date 2023/3/13 22:58
 */
public class ChannelManager {

    private final Map<String, Channel> channelMap;

    public ChannelManager() {
        channelMap = new ConcurrentHashMap<>();
    }

    public Channel get(InetSocketAddress address) {
        String key = address.toString();
        Channel channel = channelMap.get(key);
        if (channel != null && !channel.isActive()) {
            channel = null;
            channelMap.remove(key);
        }
        return channel;
    }

    public void set(InetSocketAddress address, Channel channel) {
        channelMap.put(address.toString(), channel);
    }

    public void remove(InetSocketAddress address) {
        channelMap.remove(address.toString());
    }

}
