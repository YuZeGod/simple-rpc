package top.cyuw.simplerpc.remoting.client;

import top.cyuw.simplerpc.dto.RpcResponse;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author chen
 * @date 2023/3/14 20:24
 */
public class FutureManager {

    private final Map<String, CompletableFuture<RpcResponse>> futureMap;

    public FutureManager() {
        futureMap = new ConcurrentHashMap<>();
    }

    public CompletableFuture<RpcResponse> get(String requestId) {
        return futureMap.get(requestId);
    }

    public void set(String requestId, CompletableFuture<RpcResponse> future) {
        futureMap.put(requestId, future);
    }

    public CompletableFuture<RpcResponse> remove(String requestId) {
        return futureMap.remove(requestId);
    }

}
