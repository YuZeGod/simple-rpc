package top.cyuw.simplerpc.remoting;

import top.cyuw.simplerpc.context.RpcContext;

/**
 * @author chen
 * @date 2023/3/15 21:21
 */
public interface RpcRemoting {

    void start();

    void stop();

    RpcContext getContext();

}
