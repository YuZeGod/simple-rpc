package top.cyuw.simplerpc.dto;

import lombok.*;

/**
 * @author chen
 * @date 2023/3/13 09:54
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class RpcMessage {

    private byte messageType;
    private int requestId;
    private Object body;

}
