package top.cyuw.simplerpc.dto;

import lombok.*;
import top.cyuw.simplerpc.constant.RpcConstants;

import java.io.Serializable;

/**
 * @author chen
 * @date 2023/3/13 09:54
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class RpcRequest implements Serializable {

    private static final long serialVersionUID = -7110119094873950817L;
    private String requestId;
    private String interfaceName;
    private String methodName;
    private Object[] parameters;
    private Class<?>[] paramTypes;
    private String version;
    private String group;

    public String getFullServiceName() {
        return interfaceName + RpcConstants.SERVICE_NAME_SEPARATOR + group + RpcConstants.SERVICE_NAME_SEPARATOR + version;
    }

}
