package top.cyuw.simplerpc.dto;

import lombok.*;

import java.io.Serializable;

/**
 * @author chen
 * @date 2023/3/13 09:54
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class RpcResponse implements Serializable {

    private static final long serialVersionUID = -132165431922688880L;
    private String requestId;
    private Object data;

}
