package top.cyuw.simplerpc.annotation;

import java.lang.annotation.*;

/**
 * @author chen
 * @date 2023/3/12 22:04
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface RpcConfigValue {

    String value();

}
