package top.cyuw.simplerpc.registry;

import top.cyuw.simplerpc.constant.RpcConfigKeyConstants;
import top.cyuw.simplerpc.context.RpcContext;
import top.cyuw.simplerpc.util.StringUtils;

/**
 * @author chen
 * @date 2023/3/16 21:16
 */
public class ServiceDiscoverySelector {

    private final static String DEFAULT_SERVICE_DISCOVERY_NAME = "zookeeper";

    private final static String SERVICE_DISCOVERY_NAME;

    static {
        String fromConfig = RpcContext.getInstance().getRpcConfig()
                .getString(RpcConfigKeyConstants.DISCOVERY_DEFAULT_DISCOVERY);
        if (StringUtils.isNotEmpty(fromConfig)) {
            SERVICE_DISCOVERY_NAME = fromConfig;
        } else {
            SERVICE_DISCOVERY_NAME = DEFAULT_SERVICE_DISCOVERY_NAME;
        }
    }

    public static String getServiceDiscovery() {
        return SERVICE_DISCOVERY_NAME;
    }

}
