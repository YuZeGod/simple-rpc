package top.cyuw.simplerpc.registry;

import top.cyuw.simplerpc.constant.RpcConfigKeyConstants;
import top.cyuw.simplerpc.context.RpcContext;
import top.cyuw.simplerpc.util.StringUtils;

/**
 * @author chen
 * @date 2023/3/16 21:12
 */
public class ServiceRegistrySelector {

    private final static String DEFAULT_SERVICE_REGISTRY_NAME = "zookeeper";

    private final static String SERVICE_REGISTRY_NAME;

    static {
        String fromConfig = RpcContext.getInstance().getRpcConfig()
                .getString(RpcConfigKeyConstants.REGISTRY_DEFAULT_REGISTRY);
        if (StringUtils.isNotEmpty(fromConfig)) {
            SERVICE_REGISTRY_NAME = fromConfig;
        } else {
            SERVICE_REGISTRY_NAME = DEFAULT_SERVICE_REGISTRY_NAME;
        }
    }

    public static String getServiceRegistry() {
        return SERVICE_REGISTRY_NAME;
    }

}
