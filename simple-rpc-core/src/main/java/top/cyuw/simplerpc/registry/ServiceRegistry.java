package top.cyuw.simplerpc.registry;

import top.cyuw.simplerpc.annotation.SPI;
import top.cyuw.simplerpc.config.RpcServiceConfig;

import java.net.InetSocketAddress;

/**
 * @author chen
 * @date 2023/3/16 21:00
 */
@SPI
public interface ServiceRegistry {

    void registerService(RpcServiceConfig serviceConfig, InetSocketAddress address);

    Object getService(String serviceName);

    void clear();

}
