package top.cyuw.simplerpc.registry;

import top.cyuw.simplerpc.annotation.SPI;
import top.cyuw.simplerpc.dto.RpcRequest;

import java.net.InetSocketAddress;

/**
 * @author chen
 * @date 2023/3/16 21:01
 */
@SPI
public interface ServiceDiscovery {

    InetSocketAddress lookupService(RpcRequest rpcRequest);

}
