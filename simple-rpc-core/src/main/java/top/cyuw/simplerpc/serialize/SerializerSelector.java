package top.cyuw.simplerpc.serialize;

import top.cyuw.simplerpc.constant.RpcConfigKeyConstants;
import top.cyuw.simplerpc.context.RpcContext;
import top.cyuw.simplerpc.util.StringUtils;

/**
 * @author chen
 * @date 2023/3/12 21:32
 */
public class SerializerSelector {

    private final static String DEFAULT_SERIALIZER_NAME = "kryo";

    private final static String SERIALIZER_NAME;

    static {
        String fromConfig = RpcContext.getInstance().getRpcConfig()
                .getString(RpcConfigKeyConstants.SERIALIZER_DEFAULT_SERIALIZER);
        if (StringUtils.isNotEmpty(fromConfig)) {
            SERIALIZER_NAME = fromConfig;
        } else {
            SERIALIZER_NAME = DEFAULT_SERIALIZER_NAME;
        }
    }

    public static String getSerializer() {
        return SERIALIZER_NAME;
    }

}
