package top.cyuw.simplerpc.serialize;

import top.cyuw.simplerpc.annotation.SPI;

/**
 * @author chen
 * @date 2023/3/12 21:42
 */
@SPI
public interface Serializer {

    byte[] serialize(Object obj);

    <T> T deserialize(byte[] bytes, Class<T> clazz);

}
