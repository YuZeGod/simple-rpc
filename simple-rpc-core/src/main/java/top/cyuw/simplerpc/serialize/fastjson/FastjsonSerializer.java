package top.cyuw.simplerpc.serialize.fastjson;

import com.alibaba.fastjson2.JSON;
import top.cyuw.simplerpc.serialize.Serializer;

import java.nio.charset.StandardCharsets;

/**
 * @author chen
 * @date 2023/3/12 21:49
 */
public class FastjsonSerializer implements Serializer {

    @Override
    public byte[] serialize(Object obj) {
        return JSON.toJSONString(obj).getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public <T> T deserialize(byte[] bytes, Class<T> clazz) {
        String str = new String(bytes, StandardCharsets.UTF_8);
        return JSON.parseObject(str, clazz);
    }

}
