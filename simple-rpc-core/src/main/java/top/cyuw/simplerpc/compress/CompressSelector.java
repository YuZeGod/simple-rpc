package top.cyuw.simplerpc.compress;

import top.cyuw.simplerpc.constant.RpcConfigKeyConstants;
import top.cyuw.simplerpc.context.RpcContext;
import top.cyuw.simplerpc.util.StringUtils;

/**
 * @author chen
 * @date 2023/3/12 21:32
 */
public class CompressSelector {

    private final static String DEFAULT_COMPRESS_NAME = "fastjson";

    private final static String COMPRESS_NAME;

    static {
        String fromConfig = RpcContext.getInstance().getRpcConfig()
                .getString(RpcConfigKeyConstants.COMPRESS_DEFAULT_COMPRESS);
        if (StringUtils.isNotEmpty(fromConfig)) {
            COMPRESS_NAME = fromConfig;
        } else {
            COMPRESS_NAME = DEFAULT_COMPRESS_NAME;
        }
    }

    public static String getCompress() {
        return COMPRESS_NAME;
    }

}
