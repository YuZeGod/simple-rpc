package top.cyuw.simplerpc.compress;

import top.cyuw.simplerpc.annotation.SPI;

/**
 * @author chen
 * @date 2023/3/12 21:06
 */
@SPI
public interface Compress {

    byte[] compress(byte[] bytes);

    byte[] decompress(byte[] bytes);

}
