package top.cyuw.simplerpc.config;

import top.cyuw.simplerpc.annotation.RpcConfigValue;
import top.cyuw.simplerpc.exception.RpcException;

import java.lang.reflect.Field;
import java.util.Properties;

/**
 * @author chen
 * @date 2023/3/12 22:01
 */
public abstract class AbstractConfig {

    public void loadConfig(Properties properties) {
        for (Field field : getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(RpcConfigValue.class)) {
                RpcConfigValue configValue = field.getAnnotation(RpcConfigValue.class);
                boolean accessible = field.isAccessible();
                try {
                    field.setAccessible(true);
                    Object obj = properties.get(configValue.value());
                    if (obj instanceof String) {
                        if (Integer.class.equals(field.getType())) {
                            obj = Integer.parseInt((String) obj);
                        }
                    }
                    field.set(this, obj);
                } catch (Exception e) {
                    throw new RpcException("get config value failed: " + e.getMessage(), e);
                } finally {
                    field.setAccessible(accessible);
                }
            }
        }
    }

    public String getString(String key) {
        Object obj = get(key);
        return obj instanceof String ? (String) obj : null;
    }

    public String getString(String key, String defaultValue) {
        Object obj = get(key);
        return obj instanceof String ? (String) obj : defaultValue;
    }

    public Integer getInteger(String key) {
        Object obj = get(key);
        return obj instanceof Integer ? (Integer) obj : null;
    }

    public Integer getInteger(String key, Integer defaultValue) {
        Object obj = get(key);
        return obj instanceof Integer ? (Integer) obj : defaultValue;
    }

    public Boolean getBoolean(String key) {
        Object obj = get(key);
        return obj instanceof Boolean ? (Boolean) obj : null;
    }

    public Boolean getBoolean(String key, Boolean defaultValue) {
        Object obj = get(key);
        return obj instanceof Boolean ? (Boolean) obj : defaultValue;
    }

    private Object get(String key) {
        for (Field field : getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(RpcConfigValue.class)) {
                RpcConfigValue configValue = field.getAnnotation(RpcConfigValue.class);
                if (configValue.value().equals(key)) {
                    boolean accessible = field.isAccessible();
                    try {
                        field.setAccessible(true);
                        return field.get(this);
                    } catch (Exception e) {
                        throw new RpcException("get config value failed: " + e.getMessage(), e);
                    } finally {
                        field.setAccessible(accessible);
                    }
                }
            }
        }
        return null;
    }

}
