package top.cyuw.simplerpc.config;

import lombok.Getter;
import lombok.Setter;
import top.cyuw.simplerpc.annotation.RpcConfigValue;
import top.cyuw.simplerpc.constant.RpcConfigKeyConstants;

/**
 * @author chen
 * @date 2023/3/12 22:11
 */
@Getter
@Setter
public class RpcConfig extends AbstractConfig {

    @RpcConfigValue(RpcConfigKeyConstants.SCAN_BASE_PACKAGES)
    private String scanBasePackages;

    @RpcConfigValue(RpcConfigKeyConstants.SERVER_PORT)
    private Integer serverPort;

    @RpcConfigValue(RpcConfigKeyConstants.COMPRESS_DEFAULT_COMPRESS)
    private String defaultCompress;

    @RpcConfigValue(RpcConfigKeyConstants.COMPRESS_GZIP_BUFFER_SIZE)
    private Integer gzipBufferSize;

    @RpcConfigValue(RpcConfigKeyConstants.SERIALIZER_DEFAULT_SERIALIZER)
    private String defaultSerializer;

    @RpcConfigValue(RpcConfigKeyConstants.DISCOVERY_DEFAULT_DISCOVERY)
    private String defaultDiscovery;

    @RpcConfigValue(RpcConfigKeyConstants.REGISTRY_DEFAULT_REGISTRY)
    private String defaultRegistry;

    @RpcConfigValue(RpcConfigKeyConstants.REQUEST_TIMEOUT)
    private Integer requstTimeout;

    @RpcConfigValue(RpcConfigKeyConstants.ZOOKEEPER_IP)
    private String zookeeperIp;

    @RpcConfigValue(RpcConfigKeyConstants.ZOOKEEPER_PORT)
    private Integer zookeeperPort;

    @RpcConfigValue(RpcConfigKeyConstants.LOADBALANCE_DEFAULT_LOADBALANCER)
    private String defaultLoadBalancer;

}
