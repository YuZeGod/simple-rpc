package top.cyuw.simplerpc.config;

import lombok.*;
import top.cyuw.simplerpc.constant.RpcConstants;

/**
 * @author chen
 * @date 2023/3/12 20:58
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class RpcServiceConfig {

    private String version;

    private String group;

    private Object service;

    public String getFullServiceName() {
        return getServiceName() + RpcConstants.SERVICE_NAME_SEPARATOR + getGroup() + RpcConstants.SERVICE_NAME_SEPARATOR + getVersion();
    }

    public String getServiceName() {
        return service.getClass().getInterfaces()[0].getCanonicalName();
    }

}
