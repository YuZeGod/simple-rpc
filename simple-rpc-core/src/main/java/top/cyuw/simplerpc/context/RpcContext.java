package top.cyuw.simplerpc.context;

import top.cyuw.simplerpc.config.RpcConfig;
import top.cyuw.simplerpc.exception.RpcException;

import java.io.InputStream;
import java.util.Properties;

/**
 * @author chen
 * @date 2023/3/12 22:08
 */
public class RpcContext {
    private static volatile RpcContext instance;
    private final RpcConfig rpcConfig = new RpcConfig();

    private RpcContext() {
        doInit();
    }

    public static RpcContext getInstance() {
        if (instance == null) {
            synchronized (RpcContext.class) {
                if (instance == null) {
                    instance = new RpcContext();
                }
            }
        }
        return instance;
    }

    public RpcConfig getRpcConfig() {
        return rpcConfig;
    }

    private void doInit() {
        try {
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("simple-rpc.properties");
            Properties properties = new Properties();
            properties.load(inputStream);
            rpcConfig.loadConfig(properties);
        } catch (Exception e) {
            throw new RpcException("load config failed: " + e.getMessage(), e);
        }
    }

}
