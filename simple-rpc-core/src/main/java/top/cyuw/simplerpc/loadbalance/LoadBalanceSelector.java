package top.cyuw.simplerpc.loadbalance;

import top.cyuw.simplerpc.constant.RpcConfigKeyConstants;
import top.cyuw.simplerpc.context.RpcContext;
import top.cyuw.simplerpc.util.StringUtils;

/**
 * @author chen
 * @date 2023/3/16 21:12
 */
public class LoadBalanceSelector {

    private final static String DEFAULT_LOADBALANCER_NAME = "zookeeper";

    private final static String SERVICE_LOADBALANCER_NAME;

    static {
        String fromConfig = RpcContext.getInstance().getRpcConfig()
                .getString(RpcConfigKeyConstants.LOADBALANCE_DEFAULT_LOADBALANCER);
        if (StringUtils.isNotEmpty(fromConfig)) {
            SERVICE_LOADBALANCER_NAME = fromConfig;
        } else {
            SERVICE_LOADBALANCER_NAME = DEFAULT_LOADBALANCER_NAME;
        }
    }

    public static String getLoadBalancer() {
        return SERVICE_LOADBALANCER_NAME;
    }

}
