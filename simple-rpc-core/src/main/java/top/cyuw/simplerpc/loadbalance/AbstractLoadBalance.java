package top.cyuw.simplerpc.loadbalance;

import top.cyuw.simplerpc.URL;
import top.cyuw.simplerpc.dto.RpcRequest;
import top.cyuw.simplerpc.util.CollectionUtils;

import java.util.List;

/**
 * @author chen
 * @date 2023/3/16 22:44
 */
public abstract class AbstractLoadBalance implements LoadBalance {
    @Override
    public URL select(List<URL> addressList, RpcRequest rpcRequest) {
        if (CollectionUtils.isEmpty(addressList)) {
            return null;
        }
        if (addressList.size() == 1) {
            return addressList.get(0);
        }
        return doSelect(addressList, rpcRequest);
    }

    protected abstract URL doSelect(List<URL> addressList, RpcRequest rpcRequest);
}
