package top.cyuw.simplerpc.loadbalance.loadbalancer;

import top.cyuw.simplerpc.URL;
import top.cyuw.simplerpc.dto.RpcRequest;
import top.cyuw.simplerpc.loadbalance.AbstractLoadBalance;

import java.util.List;
import java.util.Random;

/**
 * @author chen
 * @date 2023/3/16 22:43
 */
public class RandomLoadBalance extends AbstractLoadBalance {

    private final Random random;

    public RandomLoadBalance() {
        random = new Random();
    }

    @Override
    protected URL doSelect(List<URL> addressList, RpcRequest rpcRequest) {
        return addressList.get(random.nextInt(addressList.size()));
    }

}
