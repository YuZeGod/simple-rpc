package top.cyuw.simplerpc.loadbalance;

import top.cyuw.simplerpc.URL;
import top.cyuw.simplerpc.annotation.SPI;
import top.cyuw.simplerpc.dto.RpcRequest;

import java.util.List;

/**
 * @author chen
 * @date 2023/3/16 22:39
 */
@SPI
public interface LoadBalance {

    URL select(List<URL> addressList, RpcRequest rpcRequest);

}
