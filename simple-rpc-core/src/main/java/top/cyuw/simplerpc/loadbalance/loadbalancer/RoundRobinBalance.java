package top.cyuw.simplerpc.loadbalance.loadbalancer;

import top.cyuw.simplerpc.URL;
import top.cyuw.simplerpc.dto.RpcRequest;
import top.cyuw.simplerpc.loadbalance.AbstractLoadBalance;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author chen
 * @date 2023/3/17 21:09
 */
public class RoundRobinBalance extends AbstractLoadBalance {

    private final AtomicInteger counter;

    public RoundRobinBalance() {
        counter = new AtomicInteger(0);
    }

    @Override
    protected URL doSelect(List<URL> addressList, RpcRequest rpcRequest) {
        counter.set(counter.incrementAndGet() % addressList.size());
        System.out.println(addressList);
        return addressList.get(counter.get());
    }

}
