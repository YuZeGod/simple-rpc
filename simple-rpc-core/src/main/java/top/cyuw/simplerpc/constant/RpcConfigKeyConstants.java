package top.cyuw.simplerpc.constant;

/**
 * @author chen
 * @date 2023/3/12 20:15
 */
public interface RpcConfigKeyConstants {

    String SCAN_BASE_PACKAGES = "simple-rpc.scan.base-packages";

    String SERVER_PORT = "simple-rpc.server.port";

    String COMPRESS_DEFAULT_COMPRESS = "simple-rpc.compress.default-compress";

    String COMPRESS_GZIP_BUFFER_SIZE = "simple-rpc.compress.gzip.buffer-size";

    String SERIALIZER_DEFAULT_SERIALIZER = "simple-rpc.serializer.default-serializer";

    String DISCOVERY_DEFAULT_DISCOVERY = "simple-rpc.discovery.default-discovery";

    String REGISTRY_DEFAULT_REGISTRY = "simple-rpc.registry.default-registry";

    String REQUEST_TIMEOUT = "simple-rpc.request.timeout";

    String ZOOKEEPER_IP = "simple-rpc.zookeeper.ip";

    String ZOOKEEPER_PORT = "simple-rpc.zookeeper.port";

    String LOADBALANCE_DEFAULT_LOADBALANCER = "simple-rpc.loadbalance.default-loadbalancer";

}
