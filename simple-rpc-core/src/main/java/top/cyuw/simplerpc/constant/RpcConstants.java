package top.cyuw.simplerpc.constant;

/**
 * @author chen
 * @date 2023/3/13 10:11
 */
public interface RpcConstants {

    String URL_PROTOCOL = "simplerpc";

    String URL_SEPARATOR = "/";

    byte[] MAGIC_NUMBER = new byte[]{ 's', 'r', 'p', 'c' };

    byte VERSION = 0x01;

    int MAX_FRAME_LENGTH = 4 * 1024 * 1024;

    byte LENGTH_MAGIC_NUMBER = 4;

    byte LENGTH_VERSION = 1;

    byte LENGTH_FULL_LENGTH = 4;

    byte LENGTH_MESSAGE_TYPE = 1;

    byte LENGTH_REQUEST_ID = 4;

    byte LENGTH_HEAD = LENGTH_MAGIC_NUMBER + LENGTH_VERSION + LENGTH_FULL_LENGTH + LENGTH_MESSAGE_TYPE + LENGTH_REQUEST_ID;

    byte MESSAGE_TYPE_HEARTBEAT_PING = 0x00;

    byte MESSAGE_TYPE_HEARTBEAT_PONG = 0x01;

    byte MESSAGE_TYPE_RPC_REQUEST = 0x02;

    byte MESSAGE_TYPE_RPC_RESPONSE = 0x03;

    String SERVICE_NAME_SEPARATOR = "_";

}
