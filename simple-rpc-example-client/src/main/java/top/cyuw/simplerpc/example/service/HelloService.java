package top.cyuw.simplerpc.example.service;

/**
 * @author chen
 * @date 2023/3/14 20:56
 */
public interface HelloService {

    String sayHello(String msg);

}
