package top.cyuw.simplerpc.example.client;

import top.cyuw.simplerpc.config.RpcServiceConfig;
import top.cyuw.simplerpc.example.service.HelloService;
import top.cyuw.simplerpc.proxy.RpcClientProxy;
import top.cyuw.simplerpc.remoting.client.SimpleRpcClient;

/**
 * @author chen
 * @date 2023/3/14 19:56
 */
public class TestClient {

    public static void main(String[] args) {
        SimpleRpcClient rpcClient = new SimpleRpcClient();
        rpcClient.start();

        RpcServiceConfig serviceConfig = RpcServiceConfig.builder()
                .version("1.0.0")
                .group("test")
                .build();

        HelloService helloService = new RpcClientProxy(rpcClient, serviceConfig).getProxy(HelloService.class);

        while (true) {
            try {
                String hello = helloService.sayHello("hello");
                System.out.println(hello);
            } catch (Throwable t) {
                t.printStackTrace();
            }
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

}
